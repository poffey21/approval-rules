import requests
from envparse import Env
from gitlab import Gitlab as GitLab
import os
from urllib import parse
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

DEBUG_COUNT = 50

ci_yaml = {}
if os.path.isfile('.gitlab-ci.yml'):
    ci_yaml = load(open('.gitlab-ci.yml', 'r'), Loader=Loader)

data = {}
if os.path.isfile('rules.yml'):
    data = load(open('rules.yml', 'r'), Loader=Loader)

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')
private_token = env('PRIVATE_TOKEN', default=ci_yaml.get('variables', {}).get('PRIVATE_TOKEN', {}.get('value', '')))
origin = env('GITLAB_HOST', default=ci_yaml.get('variables', {}).get('GITLAB_HOST', {}.get('value', '')))
fail_when_discrepancies_fixed = env('FAIL_WHEN_DISCREPANCIES_FIXED', default=ci_yaml.get('variables', {}).get('FAIL_WHEN_DISCREPANCIES_FIXED', {}).get('value', False), cast=bool)
if fail_when_discrepancies_fixed in [True, 'true', 'True', 'TRUE']:
    fail_when_discrepancies_fixed = True
else:
    fail_when_discrepancies_fixed = False


def resolve_id(api_type, unquoted_value, sub_api_type=None, sub_api_name=None):
    try:
        group_path = parse.quote_plus(str(unquoted_value))
    except TypeError:
        print('Exepected an unquoted value but received:', unquoted_value)
        group_path = unquoted_value
    headers = {
        'PRIVATE-TOKEN': private_token,
    }
    api_id = None
    if sub_api_type is None and sub_api_name is None:
        resp = requests.get(f'{origin}/api/v4/{api_type}/{group_path}', headers=headers)
        if 'id' in resp.json():
            api_id = resp.json()['id']
        else:
            resp = requests.get(f'{origin}/api/v4/{api_type}/?search={unquoted_value}', headers=headers)
            for result in resp.json():
                if result.get('username', result.get('name')) == unquoted_value:
                    api_id = result['id']
                    break
            else:
                print(f'could not find id in response when looking in {api_type} for {unquoted_value}')
                print(resp.json())
                exit(1)
    elif sub_api_type is not None and sub_api_name is not None:
        # Todo: account for the need to paginate
        url = f'{origin}/api/v4/{api_type}/{group_path}/{sub_api_type}'
        resp = requests.get(url, headers=headers)
        if not resp.ok:
            print(url)
            print(resp.json())
            exit(1)
        for sub_api in resp.json():
            if sub_api['name'] == sub_api_name:
                api_id = sub_api['id']
                break
            if str(sub_api['id']) == str(sub_api_name):
                api_id = sub_api['id']
                break
    if api_id is None:
        print(f'Could not find a valid id in the {sub_api_type} with the name ({sub_api_name}) in the response')
        print(resp.json())
        exit(1)
    return api_id


def update_with_put(api_type, unquoted_value, sub_api_type, sub_api_id, update_payload):
    headers = {
        'PRIVATE-TOKEN': private_token,
    }
    resp = requests.put(
        f'{origin}/api/v4/{api_type}/{unquoted_value}/{sub_api_type}/{sub_api_id}',
        json=update_payload, headers=headers
    )
    if resp.ok:
        return
    else:
        print('response not okay')
        print(resp.json())
        exit(4)

def load_rules_from_yaml():
    """

    :return:
    """
    rules = []
    for rule in data['approval_rules']:
        obj = rule.copy()
        obj['user_ids'] = []
        for user in rule['users']:
            obj['user_ids'].append(resolve_id('users', user))
        obj.pop('users')
        obj['group_ids'] = []
        for group in rule['groups']:
            obj['group_ids'].append(resolve_id('groups', group))
        obj.pop('groups')
        assert(isinstance(rule['protected_branches'], list))
        assert(len(rule['protected_branches']) == 1)
        rules.append(obj)
    return rules


def main():
    root_groups = data['root_groups']
    debug = env('DEBUG', cast=bool, default=False)
    gl = GitLab(url=origin, private_token=private_token)
    gl.auth()
    rules_index = load_rules_from_yaml()
    # branches_index = load_branches_from_yaml()
    discrepancies = {}
    for group in root_groups:
        group_id = resolve_id('groups', group)
        all_groups = [gl.groups.get(group_id)] + gl.groups.get(group_id).descendant_groups.list(all=True)
        for subgroup in all_groups:
            for p in subgroup.projects.list(all=True):
                project = gl.projects.get(p.id)
                discrepancies[project.name_with_namespace] = {}
                discrepancies[project.name_with_namespace]['approval_rules'] = {}
                print(f'Looking at {p.name_with_namespace} for approval rules')

                p_mr_approvals = project.approvalrules.list()
                if not p_mr_approvals:
                    for rule in rules_index:
                        discrepancies[project.name_with_namespace]['approval_rules'][rule['name']] = 'missing'
                        discrepancies[project.name_with_namespace]['approval_rules']['url'] = (
                            f'{project.web_url}/edit#js-merge-request-approval-settings'
                        )
                        project.approvalrules.create({
                            "name": rule['name'],
                            "approvals_required": rule['approvals_required'],
                            "rule_type": "regular",
                            "user_ids": rule['user_ids'],
                            "group_ids": rule['group_ids'],
                        })
                else:
                    for rule in rules_index:
                        for actual in p_mr_approvals:
                            if actual.name == rule['name']:
                                update_payload = {}
                                expected_user_ids = list(sorted(rule['user_ids']))
                                actual_user_ids = list(sorted([x['id'] for x in actual.users]))
                                expected_group_ids = list(sorted(rule['group_ids']))
                                actual_group_ids = list(sorted([x['id'] for x in actual.groups]))

                                # The branch to protect is going to be project specific, so we have to
                                # grab that at time of getting into the project.
                                expected_protected_branch_ids = list(sorted(
                                    [
                                        resolve_id(
                                            'projects', project.id, 'protected_branches', x
                                        ) for x in rule['protected_branches']
                                    ]))
                                actual_protected_branch_ids = list(sorted([x['id'] for x in actual.protected_branches]))

                                if actual.approvals_required != rule['approvals_required']:
                                    discrepancies[project.name_with_namespace]['approval_rules']['approvals_required'] = f"({actual.approvals_required}) --> ({rule['approvals_required']})"
                                    update_payload['approvals_required'] = rule['approvals_required']
                                if expected_user_ids != actual_user_ids:
                                    discrepancies[project.name_with_namespace]['approval_rules']['user_ids'] = f'({actual_user_ids}) vs {expected_user_ids}'
                                    update_payload['user_ids'] = expected_user_ids
                                if expected_group_ids != actual_group_ids:
                                    discrepancies[project.name_with_namespace]['approval_rules']['group_ids'] = (
                                        f'({actual_group_ids}) vs {expected_group_ids}'
                                    )
                                    update_payload['group_ids'] = expected_group_ids
                                if expected_protected_branch_ids != actual_protected_branch_ids:
                                    discrepancies[project.name_with_namespace]['approval_rules']['protected_branch_ids'] = (
                                        f'({actual_protected_branch_ids}) vs {expected_protected_branch_ids}'
                                    )
                                    update_payload['protected_branch_ids'] = expected_protected_branch_ids
                                if update_payload:
                                    update_with_put('projects', project.id, 'approval_rules', actual.id, update_payload)
                                    discrepancies[project.name_with_namespace]['approval_rules']['url'] = (
                                        f'{project.web_url}/edit#js-merge-request-approval-settings'
                                    )
                                else:
                                    discrepancies[project.name_with_namespace]['approval_rules'] = {}
                                break
                        else:
                            discrepancies[project.name_with_namespace]['approval_rules'][rule['name']] = 'missing'
                            project.approvalrules.create({
                                "name": rule['name'],
                                "approvals_required": rule['approvals_required'],
                                "rule_type": "regular",
                                "user_ids": rule['user_ids'],
                                "group_ids": rule['group_ids'],
                            })

    discrepancies_found = False
    for k, v in discrepancies.items():
        if v.get('approval_rules', {}):
            print(f'Approval Rules modified for {k}:')
            print(v.get('approval_rules', {}))
            discrepancies_found = True
        else:
            print(f'No Approval Rules modified for {k}:')

    if discrepancies_found and fail_when_discrepancies_fixed:
        exit(255)


if __name__ == "__main__":
    main()

