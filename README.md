# Approval Rules

Run a scan and traverse through all projects to ensure that a particular approval rule is setup. 

## Getting started

The intent of this is to be able to run a CI Pipeline that can be run via the UI or Scheduled 
and regularly ensure projects have code reviews setup.
